#include "pch.h"
#include <iostream>
#include <algorithm>
#include <string>


// LISTA 2

// Zadanie 3

template <typename int Length, typename int x>
class Cube {
public:
	enum {
		value = Length * Cube<Length, x - 1>::value
	};

};
template<int Length>
class Cube< Length, 1 > {
public:
	enum { value = Length };
};

template <typename type3, typename type2>
auto add1(type3 *x, type2 *y) -> decltype(*x + *y)
{
	return *x + *y;
}

std::string add1(const char *x, const char *y)
{
	std::string zmienna = x;
	std::string zmienna2 = y;

	return zmienna + zmienna2;
}
int main() {
	int a = 5;
	double b = 4.4;
	int *zmienna = &a;
	double *zmienna2 = &b;
	std::cout << add1("witaj", "witaj2") << std::endl;
	std::cout << add1(zmienna, zmienna) << std::endl;
	std::cout << Cube<2, 5>::value << std::endl;
}
