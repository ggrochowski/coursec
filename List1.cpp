// C++.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include <iostream>
#include <algorithm>
#include <string>
// ZADANIE 1
template <typename type, typename type2> 
auto add1( type x, type2 y) ->
decltype(x+y)
{
	return x + y;
}


// Zadanie 2


 template <typename type, typename type2, typename type3> auto add2(type x, type2 y, type3 f) -> decltype(f(x,y)) {
	 return f(x, y);
 }

 // ZADANIE 3
 template <typename T , int size, T variable > class Vector {

 public:
	 using value_type = T;
	 Vector(){ 
		 
		 for(int i=0; i<size;i++) {
			 array[i] = variable;
			 
		}
		 
	 }
	 value_type& operator[] (int size){
		 return array[size];
	 }

	 const value_type& operator[] (const int size) const
		  {
			 return array[size];
		 }
 
 private:
	 value_type array[size];
	 
 };

// ZADANIE 4

 template <typename type, typename type2>
 typename type::value_type operator* (const type &array1, type2 const &array2) {
	typename type::value_type sum=0;
	 for (int i = 0; i < 3; i++) {
		 sum +=  (array1[i] * array2[i]);
	 }
	 return sum;
 }

 // LISTA 2

 
 int main()
 {

	 /*
	 Vector<int, 3,2>wektor;
	 Vector<int, 3,1>wektor2;
	std::cout << wektor*wektor2 << std::endl;
	std::cout << wektor[0] << std::endl;
	std::cout << add2(2.2, 2, [](int x, int y) { return x + y; });
	std::cout << add1<double>(2, 2.5) << " " << add1(2, 2) << std::endl;
	std::cout << add1<double>(2, 2.5) << " " << add1(2, 2) << std::endl;
	 */
	 
}

